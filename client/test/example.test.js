// @flow

import * as React from 'react';
import { TaskList, TaskNew, TaskDetails, TaskEdit } from '../src/task-components';
import { type Task } from '../src/task-service';
import { Component } from 'react-simplified';
import { Alert } from '../src/widgets.js';
import { shallow } from 'enzyme';
import { Form, Button, Column } from '../src/widgets';
import { NavLink } from 'react-router-dom';

describe('Alert tests', () => {
  test('Show three alert messages and close second alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test1');
    Alert.danger('test2');
    Alert.danger('test3');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsMatchingElement(
          <>
            <div>
              test1<button>&times;</button>
            </div>
            <div>
              test2<button>&times;</button>
            </div>
            <div>
              test3<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);
      wrapper.find('button.close').at(1).simulate('click');

      expect(
        wrapper.matchesElement(
          <>
            <div>
              test1<button>&times;</button>
            </div>
            <div>
              test3<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      done();
    });
  });
});

describe('TaskDetails test', () => {
  test('Draws Correctly - Renders a specific task', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          { title: 'Progge', description: 'Husk å slå på VPN', done: false },
        ])
      ).toEqual(true);

      done();
    });
  });

  test('TaskDetails draws correctly with snapshot', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    // Wait for events to complete
    setTimeout(() => {
      expect(wrapper).toMatchSnapshot();

      done();
    });
  });
});
