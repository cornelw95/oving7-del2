// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Task = {
  id: number,
  title: string,
  done: boolean,
  description: String,
};

class TaskService {
  /**
   * Get task with given id.
   */
  get(id: number) {
    return axios.get<Task>('/tasks/' + id).then((response) => response.data);
  }

  /**
   * Get all tasks.
   */
  getAll() {
    return axios.get<Task[]>('/tasks').then((response) => response.data);
  }

  /**
   * Create new task having the given title.
   *
   * Resolves the newly created task id.
   */
  create(title: string, description: string) {
    return axios
      .post<{}, { id: number }>('/tasks', { title: title, description: description })
      .then((response) => response.data.id);
  }

  //Lagt til av meg:

  save(id: number, title: String, description: String, done: Boolean) {
    return axios
      .put<{}, { id: number }>('/tasks/' + id, {
        title: title,
        description: description,
        done: done,
      })
      .then((response) => console.log('Element saved'));
  }

  delete(id: number) {
    return axios.delete<Task>('/tasks/' + id).then((response) => console.log('Element deleted'));
  }

  checked(id: number) {
    return axios.put<{}, {}>(`/tasks/${id}`);
  }
}

const taskService = new TaskService();
export default taskService;
